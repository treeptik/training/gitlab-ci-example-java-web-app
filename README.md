# Example Java Web App

Example of using GitLab CI to continuously build and deploy a Java Web App

## Versions

- Deployment of a Java JAR: see `jar-deployment` branch
- Deployment of a Docker container image: see `docker-deployment` branch.